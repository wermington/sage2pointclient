/**
 * @author stanko
 * @date 3/5/15
 */



function WebsocketClient(socketUrl)
{
    this.socketUrl = socketUrl;
    this.websocketConnection = null;
    this.container = new ObjectContainer();
}


WebsocketClient.prototype.connect = function()
{
    this.websocketConnection = new WebSocket(this.socketUrl);
    this.websocketConnection.onopen = function(event)
    {
        console.log("Connected to server ... ");
    }
    var _this = this;
    this.websocketConnection.onmessage = function(event){ _this.messageHandler(event); }
}


WebsocketClient.prototype.messageHandler = function(event)
{
    var message = event.data;
    console.log(message);
    try
    {
        var split = message.split(" ");
        var type = split.shift();
        var object = split.join(" ");
        if(type != "P" && type != "Q" && type !="RW:" && type != "RH:") return;
        var jsonObject = JSON.parse(object);
        if(type == "P") this.receivedPoint(jsonObject);
        else if(type == "Q") this.receivedSet(jsonObject);
        else if(type == "RW:") this.setResolutionWidth(object);
        else if(type == "RH:") this.setResolutionHeight(object);
    }catch (ex)
    {
        console.log(ex);
    }

}


WebsocketClient.prototype.receivedPoint = function(point)
{
    this.container.add(point.id, point.x, point.y, point.z);
}


WebsocketClient.prototype.receivedSet = function(list)
{
    this.container.removeUnactive(list);
}

WebsocketClient.prototype.setResolutionWidth = function(width)
{
    console.log("[websocket] setting Resolution Width: " + width);

    this.container.width = parseInt(width);
    this.container.touchWidth = this.container.width;
}

WebsocketClient.prototype.setResolutionHeight = function(height)
{
    console.log("[websocket] setting Resolution Height: " + height);
    this.container.height = parseInt(height);
    this.container.touchHeiht = this.container.height;
}

WebsocketClient.prototype.resize = function(width, height)
{
    console.log("[Client] (resize): " + width + ", " +height);
    this.container.height = parseInt(height);
    this.container.width = parseInt(width);
}





