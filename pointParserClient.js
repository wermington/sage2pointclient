// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

var pointParserClient = SAGE2_App.extend( {
	construct: function() {
		arguments.callee.superClass.construct.call(this);
        this.resizeEvents = "onfinish";
        this.enableControls = true;
        this.websocketClient = null;
        this.ctx = null;
	},
	
	init: function(data) {
		// call super-class 'init'
		arguments.callee.superClass.init.call(this, "canvas", data);
		
		// application specific 'init'
        this.ctx = this.element.getContext("2d");
		console.log("Size ", this.element.clientWidth, this.element.clientHeight);
        try {
            this.websocketClient = new WebsocketClient("ws://localhost:10888/pointParser");
            this.websocketClient.connect();
        }catch (exception)
        {
            console.log("[init] (Websocket): " + exception);
        }
        this.controls.finishedAddingControls(); //Not adding controls but making the default buttons available

    },
	
	load: function(state, date) {
	},
	
	draw: function(date) {


        var ewidth = this.element.width;
        var eheight = this.element.height;
        this.websocketClient.container.width = ewidth;
        this.websocketClient.container.height = eheight;
        try {


            this.ctx.clearRect(0, 0, ewidth, eheight);
            this.ctx.fillStyle = "white";
            this.ctx.fillRect(0, 0, ewidth, eheight);


            this.websocketClient.container.drawAll(this.ctx);

        }catch (exception)
        {
            console.log("Exeption [draw]: " + exception);
        }
	},
	
	resize: function(date) {
        console.log("resize> " + this.element.width + " : " + this.element.height);
        this.websocketClient.container.width = this.element.width;
        this.websocketClient.container.height = this.element.height;
		this.refresh(date);

	},
	
	event: function(eventType, position, user_id, data, date) {
	}
});
