function ObjectContainer()
{
    this.points = [];
    this.width = 1000.0;
    this.height = 700.0;
    this.touchWidth = 1000.0;
    this.touchHeiht = 700.0;
}


ObjectContainer.prototype.deleteObject = function(id)
{
    var obj = this.get(id);

    if(obj == null) { 
	    console.log("No object with id == " + id);
 	    return;
    }

    var index = this.points.indexOf(obj);
    console.log("Slicing object with id: " + id);
    this.points.slice(index, 1);
}


ObjectContainer.prototype.add = function(id, x, y ,z)
{
    if(id == -1) return;
    var obj = this.get(id);
    if(obj == null){
        this.points.push(new PointObject(id, x, y, z));
    }else
    {
        obj.x = x;
        obj.y = y;
        obj.z = z;
    }
    console.log("Added object: " + point);
    this.points.push(new PointObject(id, x, y, z));
}


ObjectContainer.prototype.get = function (id)
{
    if(id == -1) return null;
    for(var i =0; i < this.points.length; i++)
    {
        if(this.points[i].getId() == id)
            return this.points[i];
    }
    return null;
}


ObjectContainer.prototype.getPoints = function()
{
    return this.points;
}


ObjectContainer.prototype.removeUnactive = function(active)
{
    console.log("Active: " + active);

    var result = [];
    for(var i = 0 ; i < this.points.length; i++)
    {
        for(var j = 0; j < active.length; j++)
        {
            if(this.points[i].getId() == active[j]){
                result.push(this.points[i]);
            }

        }
    }
    console.log(result);
    this.points = result;
}


ObjectContainer.prototype.drawAll = function(gtx)
{
    gtx.fillStyle = "red";
    for(var i = 0; i < this.points.length; i++)
    {
        var point  = this.points[i];
        var ratio_x = 0.0;
        var ratio_y = 0.0;
        ratio_x =  (this.width*1.0) / this.touchWidth;

        ratio_y = (this.height*1.0) / this.touchHeiht;

        var x = point.x * ratio_x;
        var y = point.y * ratio_y;

        var radius = 50;
	
	    gtx.fillStyle = "red";
        gtx.beginPath();
        gtx.arc(x, y, radius, 0, 2*Math.PI);
        console.log("Drawing point on: [" + x + ", " + y + "]");
        gtx.fill();
	gtx.closePath();
    }
}




