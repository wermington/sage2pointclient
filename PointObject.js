/**
 * Created by stanko on 3/5/15.
 */

function PointObject(id, x, y ,z)
{
    this.x = x;
    this.y = y;
    this.z = z;
    this.id = id;
}

PointObject.prototype.getX = function()
{
    return this.x;
}

PointObject.prototype.getY = function()
{
    return this.y;
}

PointObject.prototype.getZ = function()
{
    return this.z;
}

PointObject.prototype.getId = function()
{
    return this.id;
}


PointObject.prototype.toString = function()
{
    return "[ "+this.id+", " + this.x + ", " + this.y + ", " + this.z + " ]";
}








